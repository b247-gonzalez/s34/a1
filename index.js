const { json } = require('express');
const express = require('express');

// Initialize ExpressJS by using it to the app variable as a function
const app = express();

const port = 4004;

// Use middleware to allow ExpressJS to read JSON
app.use(express.json());

// Use this middleware to allow ExpressJS to be able to read more datatypes from a response
app.use(express.urlencoded({extended: true}));

// Listen to the port and console.log a text once the server is running
app.listen(port, () => console.log(`Server is running at http://localhost:${port}`));

let users = [
    {
        "username" : "johndoe",
        "password" : "johndoe123"
    },

    {
        "username" : "kat.everdeen",
        "password" : "girlOnFire123"
    },

    {
        "username" : "tris",
        "password" : "the1Divergent"
    }
]

// [ ROUTES ]
app.get("/home", (req, res) => {
    res.send("Welcome to the home page");
});

app.get("/users", (req, res) => {
    res.send(users);
});

app.delete("/delete-user", (req, res) => {
    for (i = 0; i < users.length; i++) {
        if (req.body.username == users[i].username) {
            users.splice(i,1);
            message = `User ${req.body.username} deleted from Users database`;
            break;
        }
        else {
            message = "User not found!";
        }
    }
    res.send(message);
});

// GOAL STRETCHER!!!!!!!
const books = [
    { title: 'North', author: 'Seamus Heaney', pages: 304},
	{ title: 'To Kill a Mockingbird', author: 'Harper Lee', pages: 281 },
    { title: 'The Great Gatsby', author: 'F. Scott Fitzgerald', pages: 180 },
    { title: 'Silent Spring', author: 'Rachel Carson', pages: 400 },
    { title: 'Noli Me Tángere', author: 'José Rizal', pages: 438 },
	{ title: 'No Logo', author: 'Naomi Klein', pages: 490 },
  	{ title: '1984', author: 'George Orwell', pages: 328 },
  	{ title: 'The Sixth Extinction', author: 'Elizabeth Kolbert', pages: 391 },
  	{ title: 'Brave New World', author: 'Aldous Huxley', pages: 311 },
  	{ title: 'The Catcher in the Rye', author: 'J.D. Salinger', pages: 234 }
];

app.get("/books/total-pages", (req, res) => {
    let arrPages = books.map(obj => obj.pages);
    let total = arrPages.reduce((accu, crnt) => {
        return accu + crnt;
    });

    res.send(`The total number of pages is ${total}`);
});

app.get("/books/large-books", (req, res) => {
    let largeBooks = books.filter((el) => {
        return el.pages >= 250;
    });
    res.send(largeBooks);
});

app.get("/author", (req, res) => {
    let greatest;
    let currentIndex;
    for (i = 0; i < books.length; i++) {
        if (greatest > books[i].pages) {
            greatest = greatest;
            currentIndex = currentIndex;
        }
        else {
            greatest = books[i].pages;
            currentIndex = i;
        }
    }
    let message = `${books[currentIndex].author} is the most prolific author with ${books[currentIndex].pages} pages written`;
    res.send(message);
});